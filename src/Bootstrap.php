<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting;

use yii\helpers\ArrayHelper;

/**
 * Description of Bootstrap
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Bootstrap implements \yii\base\BootstrapInterface
{

    /**
     *
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if($app instanceof \yii\console\Application)
        {
            // добавление пути к миграциям модуля в Yii контроллер миграций
            $app->controllerMap = ArrayHelper::merge($app->controllerMap, [
                        'migrate' => [
                            'class' => \yii\console\controllers\MigrateController::class,
                            'migrationNamespaces' => ['JzWebstudio\Yii2StorageAccounting\Migrations']
                        ]
            ]);
        } else
        {
            $app->urlManager->addRules([
                [
                    'class' => \yii\web\GroupUrlRule::class,
                    'prefix' => 'products',
                    'rules' => [
                        'GET ' => 'storage-items/index',
                        'create' => 'storage-items/create',
                        'update/<uid:[\w\-\.]+>' => 'storage-items/update',
                    ]
                ]
            ],false);
        }
        $app->i18n->translations['jzwebstudio/storage'] = [
            'class' => \yii\i18n\PhpMessageSource::class,
            'sourceLanguage' => 'en',
            'forceTranslation' => true,
            'basePath' => '@JzWebstudio/Yii2StorageAccounting/messages',
            'fileMap' => [
                'storage' => 'storage.php'
            ]
        ];
    }

}
