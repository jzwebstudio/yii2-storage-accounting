<?php

namespace JzWebstudio\Yii2StorageAccounting\Migrations;

/**
 * Class m210826_054704_add_brief_column
 */
class m210826_054704_add_brief_column extends Migration
{

    protected $table = "{{%storage_item}}";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'brief', $this->text()->notNull()->defaultValue('')->after('slug'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'brief');
    }

}
