<?php

namespace JzWebstudio\Yii2StorageAccounting\Migrations;

/**
 * Class m201228_144408_create_storage_unit_tables
 */
class m201228_144408_create_storage_unit_tables extends Migration
{

    protected $itemTable = "{{%storage_item}}";
    protected $unitTable = "{{%storage_unit}}";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->itemTable, [
            'uid' => "BINARY(16) NOT NULL",
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->text()->notNull()->defaultValue(''),
            'default_variant' => $this->string(128)->notNull()
                ], $this->tableOptions);
        $this->addPrimaryKey('', $this->itemTable, 'uid');

        $this->createTable($this->unitTable, [
            'sku_number' => $this->string(128)->notNull(),
            'sku_type' => $this->string()->notNull(),
            'belongs_to' => "BINARY(16) NOT NULL",
            'title' => $this->string()->notNull(),
            'preview' => $this->string()->notNull()->defaultValue(''),
            'price' => $this->decimal(10, 2)->notNull()->defaultValue(0.00),
            'status' => $this->smallInteger()->notNull(),
            'characteristics' => $this->json()->null()->defaultValue(null),
            'available' => $this->integer()->notNull()->defaultValue(-1)
                ], $this->tableOptions);

        $this->addPrimaryKey('', $this->unitTable, 'sku_number');
        $this->createIndex('storage_item_slug', $this->itemTable, 'slug', true);
        $this->createIndex('fk_storageunit_item_idx', $this->unitTable, 'belongs_to');
        $this->addForeignKey('fk_storageunit_item', $this->unitTable, 'belongs_to', $this->itemTable, 'uid', $this->restrict, $this->restrict);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->unitTable);
        $this->dropTable($this->itemTable);
    }

}
