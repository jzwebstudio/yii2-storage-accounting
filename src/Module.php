<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting;

use Yii;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use RobotE13\StorageAccounting\Services\StorageItem\Update\{
    UpdateItem,
    UpdateItemHandler
};
use RobotE13\StorageAccounting\Services\StorageUnit\Update\{
    UpdateStorageUnit,
    UpdateStorageUnitHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\Get\{
    GetStorageItems,
    GetStorageItemsHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\AddSkuVariant\{
    AddSkuVariant,
    AddSkuVariantHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\Create\{
    CreateItem,
    CreateItemHandler,
    CreateItemValidator
};

/**
 * yii2-user-account module definition class
 */
class Module extends \yii\base\Module
{

    public $defaultRoute = 'storage-items';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'JzWebstudio\Yii2StorageAccounting\Controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if(Yii::$app instanceof \yii\console\Application)
        {
            $this->controllerNamespace = 'JzWebstudio\Yii2StorageAccounting\Console';
        }

        Yii::$container->setSingleton('commandBus', function() {
            $locator = new InMemoryLocator();
            $locator->addHandler(Yii::createObject(CreateItemHandler::class), CreateItem::class);
            $locator->addHandler(Yii::createObject(UpdateStorageUnitHandler::class), UpdateStorageUnit::class);
            $locator->addHandler(Yii::createObject(AddSkuVariantHandler::class), AddSkuVariant::class);
            $locator->addHandler(Yii::createObject(GetStorageItemsHandler::class), GetStorageItems::class);
            $locator->addHandler(Yii::createObject(UpdateItemHandler::class), UpdateItem::class);
            $middleware = [
                Yii::createObject(CreateItemValidator::class),
                new CommandHandlerMiddleware(
                        new ClassNameExtractor(),
                        $locator,
                        new HandleInflector()
                )
            ];
            return new \League\Tactician\CommandBus(
                    $middleware
            );
        });
    }

}
