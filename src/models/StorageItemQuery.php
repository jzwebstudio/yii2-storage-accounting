<?php

namespace JzWebstudio\Yii2StorageAccounting\models;

/**
 * This is the ActiveQuery class for [[StorageItem]].
 *
 * @see StorageItem
 */
class StorageItemQuery extends \yii\db\ActiveQuery
{
//    public function active()
//    {
//        return $this->andWhere('status=1');
//    }

    /**
     * {@inheritdoc}
     * @return StorageItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return StorageItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
