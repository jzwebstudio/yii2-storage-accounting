<?php

namespace JzWebstudio\Yii2StorageAccounting\models;

use Yii;

/**
 * This is the model class for table "{{%shop_category_product_link}}".
 *
 * @property int $category_uid
 * @property string $product_uid
 * @property int $position
 *
 * @property StorageItem $productU
 * @property ShopCategory $categoryU
 */
class ShopCategoryProductLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_category_product_link}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_uid', 'product_uid', 'position'], 'required'],
            [['category_uid', 'position'], 'integer'],
            [['product_uid'], 'string', 'max' => 16],
            [['category_uid', 'product_uid'], 'unique', 'targetAttribute' => ['category_uid', 'product_uid']],
            [['product_uid'], 'exist', 'skipOnError' => true, 'targetClass' => StorageItem::className(), 'targetAttribute' => ['product_uid' => 'uid']],
            [['category_uid'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['category_uid' => 'uid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_uid' => 'Category Uid',
            'product_uid' => 'Product Uid',
            'position' => 'Position',
        ];
    }

    /**
     * Gets query for [[ProductU]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductU()
    {
        return $this->hasOne(StorageItem::className(), ['uid' => 'product_uid']);
    }

    /**
     * Gets query for [[CategoryU]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryU()
    {
        return $this->hasOne(ShopCategory::className(), ['uid' => 'category_uid']);
    }
}
