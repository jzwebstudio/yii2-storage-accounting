<?php

namespace JzWebstudio\Yii2StorageAccounting\models;

use Yii;

/**
 * This is the model class for table "{{%storage_item}}".
 *
 * @property string $uid
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $default_variant
 *
 * @property ShopCategoryProductLink[] $shopCategoryProductLinks
 * @property ShopCategory[] $categoryUs
 * @property StorageUnit[] $storageUnits
 */
class StorageItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%storage_item}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'title', 'slug', 'default_variant'], 'required'],
            [['description'], 'string'],
            [['uid'], 'string', 'max' => 16],
            [['title', 'slug', 'default_variant'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['uid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'title' => 'Title',
            'slug' => 'Slug',
            'description' => 'Description',
            'default_variant' => 'Default Variant',
        ];
    }

    public function fields()
    {
        return array_merge(parent::fields(),[
            'uid' => fn()=>(new \RobotE13\StorageAccounting\Entities\Id($this->uid))->getString(),
            'storageUnits' => fn() => $this->storageUnits
        ]);
    }

    public function afterFind()
    {
        //$this->uid = (new \RobotE13\StorageAccounting\Entities\Id($this->uid))->getString();
    }

    public function hasVariants()
    {
        return count($this->storageUnits) > 1;
    }

    /**
     * Gets query for [[ShopCategoryProductLinks]].
     *
     * @return \yii\db\ActiveQuery|ShopCategoryProductLinkQuery
     */
    public function getShopCategoryProductLinks()
    {
        return $this->hasMany(ShopCategoryProductLink::className(), ['product_uid' => 'uid']);
    }

    /**
     * Gets query for [[CategoryUs]].
     *
     * @return \yii\db\ActiveQuery|ShopCategoryQuery
     */
    public function getCategoryUs()
    {
        return $this->hasMany(ShopCategory::className(), ['uid' => 'category_uid'])->viaTable('{{%shop_category_product_link}}', ['product_uid' => 'uid']);
    }

    /**
     * Gets query for [[StorageUnits]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getStorageUnits()
    {
        return $this->hasMany(StorageUnit::className(), ['belongs_to' => 'uid']);
    }

    /**
     * Gets query for [[StorageUnits]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getDefaultVariant()
    {
        return $this->hasOne(StorageUnit::className(), ['sku_number'=>'default_variant']);
    }

    /**
     * {@inheritdoc}
     * @return StorageItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StorageItemQuery(get_called_class());
    }
}
