<?php

namespace JzWebstudio\Yii2StorageAccounting\models;

use Yii;

/**
 * This is the model class for table "{{%storage_unit}}".
 *
 * @property string $sku_number
 * @property string $sku_type
 * @property string $belongs_to
 * @property string $title
 * @property string $preview
 * @property float $price
 * @property string|null $characteristics
 * @property int $available
 *
 * @property StorageItem $belongsTo
 */
class StorageUnit extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%storage_unit}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sku_number', 'sku_type', 'belongs_to', 'title'], 'required'],
            [['price'], 'number'],
            [['characteristics'], 'string'],
            [['available'], 'integer'],
            [['sku_number', 'belongs_to'], 'string', 'max' => 16],
            [['sku_type', 'title', 'preview'], 'string', 'max' => 255],
            [['sku_number'], 'unique'],
            [['belongs_to'], 'exist', 'skipOnError' => true, 'targetClass' => StorageItem::className(), 'targetAttribute' => ['belongs_to' => 'uid']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['belongs_to']);
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sku_number' => 'Sku Number',
            'sku_type' => 'Sku Type',
            'belongs_to' => 'Belongs To',
            'title' => 'Title',
            'preview' => 'Preview',
            'price' => 'Price',
            'characteristics' => 'Characteristics',
            'available' => 'Available',
        ];
    }

    public function afterFind()
    {
        //$this->belongs_to = (new \RobotE13\StorageAccounting\Entities\Id($this->belongs_to))->getString();
        $this->characteristics = json_decode($this->characteristics, true);
        $this->sku_number = preg_replace('/[^\w\-\.]/', '', $this->sku_number);
        parent::afterFind();
    }

    /**
     * Gets query for [[BelongsTo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBelongsTo()
    {
        return $this->hasOne(StorageItem::className(), ['uid' => 'belongs_to']);
    }

}
