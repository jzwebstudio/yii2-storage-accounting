<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Console;

/**
 * Description of AdminController
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class AdminController extends \yii\console\Controller
{
    public function actionTest($param)
    {
        echo $param . PHP_EOL;
    }
}
