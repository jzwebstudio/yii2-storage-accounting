<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Controllers\Backoffice;

use Yii;
use yii\web\Response;
use yii\filters\VerbFilter;
use RobotE13\Yii2RenderNegotiator\RenderNegotiator;
use RobotE13\StorageAccounting\Repositories\NotFoundException;
use JzWebstudio\Yii2StorageAccounting\Forms\UpdateUnitForm;

/**
 * Description of StorageUnitController
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageUnitController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    public function behaviors(): array
    {
        return[
            'negotiator' => [
                'class' => RenderNegotiator::class,
                'formats' => [
//                    '*/*' => Response::FORMAT_HTML,
//                    'text/html' => Response::FORMAT_HTML,
                    'application/json' => Response::FORMAT_JSON,
                ],
//                'only' => ['index', 'view']
            ],
            'verb' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'update' => ['POST']
                ]
            ]
        ];
    }

    public function actionUpdate($skuNumber)
    {
        $form = new UpdateUnitForm($skuNumber);
        $form->load(Yii::$app->request->post());
        if($form->validate())
        {
            try {
                Yii::$app->commandBus->handle($form->getCommand());
            } catch (NotFoundException $exc) {
                throw new \yii\web\NotFoundHttpException($exc->getMessage());
            }
        }

        return $this->render('update', compact('form'));
    }

}
