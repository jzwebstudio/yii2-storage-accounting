<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Controllers\Backoffice;

use Yii;
use yii\web\{
    NotFoundHttpException,
    Response
};
use RobotE13\Yii2RenderNegotiator\RenderNegotiator;
use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;
use JzWebstudio\Yii2StorageAccounting\Forms\StorageItemSearch;
use JzWebstudio\Yii2StorageAccounting\Forms\{
    CreateItemForm,
    UpdateItemForm
};

/**
 * Description of StorageItemsController
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItemsController extends \yii\web\Controller
{

    /**
     *
     * @var StorageItemRepository
     */
    private $storageItems;

    public function __construct($id, $module, StorageItemRepository $storageItems, $config = array())
    {
        $this->storageItems = $storageItems;
        parent::__construct($id, $module, $config);
    }

    public function behaviors(): array
    {
        return[
            'negotiator' => [
                'class' => RenderNegotiator::class,
                'formats' => [
                    '*/*' => Response::FORMAT_HTML,
                    'text/html' => Response::FORMAT_HTML,
                    'application/json' => Response::FORMAT_JSON,
                ],
//                'only' => ['index', 'view']
            ],
            'verb' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'update' => ['POST'],
                    'create' => ['POST']
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /* @var $result \RobotE13\StorageAccounting\Services\CommandResult */
        $search = new StorageItemSearch();
        $search->load(Yii::$app->request->get());
        $result = Yii::$container->get('commandBus')->handle($search->getCommand());
        if(!$result->isSuccessful())
        {
            array_reduce($result->getErrors(), fn($error) => $search->addError('title', $error));
            return $search;
        } else
        {
            $dataProvider = $result->getResult();
        }
        return $this->render('index', compact('search', 'dataProvider'));
    }

    public function actionUpdate($uid)
    {
        $form = new UpdateItemForm($uid);
        if($form->item->load(Yii::$app->request->post()) && $form->validate())
        {

            try {
                $result = Yii::$container->get('commandBus')->handle($form->getCommand());
            } catch (NotFoundException $exception) {
                throw new NotFoundHttpException($exception->getMessage());
            }
            if(!$result->isSuccessful())
            {
                $form->addErrors($result->getErrors());
            }
        }

        return $this->render('update', compact('form'));
    }

    public function actionCreate()
    {
        /* @var $result \RobotE13\StorageAccounting\Services\CommandResult */
        $model = new CreateItemForm();

        $model->load(Yii::$app->request->post());
        if($model->validate())
        {
            $result = Yii::$container->get('commandBus')->handle($model->getCommand());
            if(!$result->isSuccessful())
            {
                $model->addErrors($result->getErrors());
            }
        }
        $isSuccess = !$model->hasErrors();
        return $this->render('create', compact('model', 'isSuccess'));
    }

}
