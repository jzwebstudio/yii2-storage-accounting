<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Controllers;

use Yii;
use RobotE13\StorageAccounting\Repositories\NotFoundException;
use JzWebstudio\Yii2StorageAccounting\Forms\StorageItemSearch;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;

/**
 * Description of StorageItemsController
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItemsController extends \yii\web\Controller
{

    /**
     *
     * @var StorageItemRepository
     */
    private $storageItems;

    public function __construct($id, $module, StorageItemRepository $storageItems, $config = array())
    {
        $this->storageItems = $storageItems;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        /* @var $result \RobotE13\StorageAccounting\Services\CommandResult */
        $search = new StorageItemSearch();
        $search->load(Yii::$app->request->get());
        $result = Yii::$container->get('commandBus')->handle($search->getCommand());
        if(!$result->isSuccessful())
        {
            throw new \Exception($result->getFirstError());
        }else{
            $dataProvider = $result->getResult();
        }
        return $this->render('index.twig', compact('search','dataProvider'));
    }

    public function actionView($slug)
    {
        try {
            $product = $this->storageItems->findBySlug($slug);
        } catch (NotFoundException $exc) {
            throw new \yii\web\NotFoundHttpException($exc->getMessage());
        }

        return $this->render('view.twig', compact('product'));
    }

}
