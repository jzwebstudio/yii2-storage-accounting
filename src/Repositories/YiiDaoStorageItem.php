<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Repositories;

use Yii;
use yii\db\Query;
use samdark\hydrator\Hydrator;
use ProxyManager\Proxy\LazyLoadingInterface;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\UnitTypeRepository;
use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;
use RobotE13\StorageAccounting\Entities\StorageUnit\{
    StorageItem,
    StorageUnit,
    StorageUnitsCollection
};

/**
 * Description of MysqlStorageItem
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class YiiDaoStorageItem implements StorageItemRepository
{

    /**
     *
     * @var \yii\db\Connection
     */
    private $db;

    /**
     * @var Hydrator;
     */
    private $hydrator;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $lazyFactory;

    /**
     *
     * @var UnitTypeRepository
     */
    private $unitTypes;

    /**
     *
     * @param LazyLoadingValueHolderFactory $lazyFactory
     * @param UnitTypeRepository $unitTypes
     * @param string $db Yii connection component name
     */
    function __construct(LazyLoadingValueHolderFactory $lazyFactory, UnitTypeRepository $unitTypes, $db = 'db')
    {
        $this->lazyFactory = $lazyFactory;
        $this->unitTypes = $unitTypes;
        $this->db = Yii::$app->get($db);
        $this->hydrator = new Hydrator([
            'uid' => 'uid',
            'title' => 'title',
            'slug' => 'slug',
            'brief' => 'brief',
            'description' => 'description',
            'default_variant' => 'defaultVariant',
            'variants' => 'variants'
        ]);
    }

    public function add(StorageItem $item)
    {
        $this->db->createCommand()
                ->insert("{{%storage_item}}", [
                    'uid' => $item->getUid()->getBytes(),
                    'title' => $item->getTitle(),
                    'slug' => $item->getSlug(),
                    'brief' => $item->getBrief(),
                    'description' => $item->getDescription(),
                    'default_variant' => $item->getDefaultVariant()->getSkuNumber()
                ])
                ->execute();

        $this->updateVariants($item);
    }

    public function existsById($uid): bool
    {
        $factory = new \Ramsey\Uuid\UuidFactory();
        $factory->setCodec(new \Ramsey\Uuid\Codec\OrderedTimeCodec($factory->getUuidBuilder()));

        return $this->existsByCondition(['uid' => $factory->fromString($uid)->getBytes()]);
    }

    public function existsBySlug($slug): bool
    {
        return $this->existsByCondition(['slug' => $slug]);
    }

    public function findById($uid): StorageItem
    {
        if (!$this->existsById($uid))
        {
            throw new NotFoundException('Storage item not found.');
        }
        $factory = new \Ramsey\Uuid\UuidFactory();
        $factory->setCodec(new \Ramsey\Uuid\Codec\OrderedTimeCodec($factory->getUuidBuilder()));

        return $this->populate($this->findByCondition(['uid' => $factory->fromString($uid)->getBytes()]));
    }

    public function findBySlug($slug): StorageItem
    {
        if (!$this->existsBySlug($slug))
        {
            throw new NotFoundException('Storage item not found.');
        }
        return $this->populate($this->findByCondition(['slug' => $slug]));
    }

    public function findBySku($skuNumber): StorageItem
    {
        $uid = (new Query())
                ->from('{{%storage_unit}}')
                ->where(['sku_number' => $skuNumber])
                ->select('belongs_to')
                ->scalar();
        if (empty(bin2hex($uid)))
        {
            throw new NotFoundException('Storage item not found.');
        }
        $factory = new \Ramsey\Uuid\UuidFactory();
        $factory->setCodec(new \Ramsey\Uuid\Codec\OrderedTimeCodec($factory->getUuidBuilder()));
        $factory->fromBytes($uid);

        return $this->findById($factory->fromBytes($uid)->toString());
    }

    public function remove($uid): StorageItem
    {

    }

    public function update(StorageItem $item)
    {
        $this->db->createCommand()
                ->update("{{%storage_item}}", [
                    'title' => $item->getTitle(),
                    'slug' => $item->getSlug(),
                    'brief' => $item->getBrief(),
                    'description' => $item->getDescription(),
                        ], ['uid' => $item->getUid()->getBytes()])
                ->execute();
        $this->updateVariants($item);
    }

    private function findByCondition($condition)
    {
        return (new Query())
                        ->from("{{%storage_item}}")
                        ->where($condition)
                        ->one($this->db);
    }

    private function populate($row): StorageItem
    {
        $uid = new \RobotE13\StorageAccounting\Entities\Id($row['uid']);
        $row['uid'] = $uid;
        $row['variants'] = $this->lazyFactory->createProxy(StorageUnitsCollection::class,
                function (&$target, LazyLoadingInterface $proxy) use ($uid) {
            $units = (new Query())->select('*')
                    ->from("{{%storage_unit}}")
                    ->andWhere(['belongs_to' => $uid->getUuid()->getBytes()])
                    ->all($this->db);
            $target = new StorageUnitsCollection(
                    array_map(function ($unit) {
                        return new StorageUnit(
                                $unit['sku_number'],
                                $unit['title'],
                                $this->unitTypes->find($unit['sku_type']),
                                $unit['status'],
                                json_decode($unit['characteristics'], true),
                                $unit['preview'],
                                $unit['price'],
                                $unit['available']
                        );
                    }, $units));
            $proxy->setProxyInitializer(null);
        });
        return $this->hydrator->hydrate($row, StorageItem::class);
    }

    /**
     *
     * @param StorageItem $item
     * @return void
     */
    private function updateVariants($item): void
    {
        $data = $this->hydrator->extract($item);
        $variants = $data['variants'];
        if ($variants instanceof LazyLoadingInterface && !$variants->isProxyInitialized())
        {
            return;
        }

        $units = array_map(function($unit) use ($item) {
            /* @var $unit StorageUnit */
            return[
                'sku_number' => $unit->getSkuNumber(),
                'sku_type' => $unit->getSkuType()->getName(),
                'belongs_to' => $item->getUid()->getBytes(),
                'title' => $unit->getTitle(),
                'preview' => $unit->getPreview(),
                'price' => $unit->getPrice(),
                'status' => $unit->getStatus(),
                'characteristics' => json_encode($unit->getCharacteristics(),JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES,4096),
                'available' => $unit->getAvailableQuantity(),
            ];
        }, $item->getVariants()->getAll());
        if (!empty($units))
        {
            $attributes = array_keys(reset($units));
            $sql = $this->db->queryBuilder->batchInsert("{{%storage_unit}}", $attributes, $units);
            \yii\helpers\ArrayHelper::removeValue($attributes, 'sku_number');
            $command = $this->db->createCommand($sql
                    . ' ON DUPLICATE KEY UPDATE '
                    . implode(', ', array_map(
                                    fn($attribute) => "{$attribute} = VALUES({$attribute})",
                                    $attributes
                            )
                    )
            );
            $command->execute();
        }
    }

    private function existsByCondition($condition): bool
    {
        return (new Query())
                        ->from("{{%storage_item}}")
                        ->where($condition)
                        ->exists($this->db);
    }

}
