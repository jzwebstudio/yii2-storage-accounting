<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JzWebstudio\Yii2StorageAccounting\Repositories;

use Yii;
use yii\db\Query;
use yii\db\Connection;
use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\UnitTypeRepository;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageUnitRepository;

/**
 * Description of YiiDaoStorageUnit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class YiiDaoStorageUnit implements StorageUnitRepository
{

    /**
     *
     * @var Query
     */
    private $query;

    /**
     *
     * @var UnitTypeRepository
     */
    private $unitTypes;

    /**
     *
     * @var Connection
     */
    private $db;

    public function __construct(Query $query, UnitTypeRepository $unitTypes, string $db = 'db')
    {
        $this->unitTypes = $unitTypes;
        $this->query = $query;
        $this->db = Yii::$app->get($db);
    }

    public function exists($skuNumber): bool
    {
        return $this->query
                        ->from('{{%storage_unit}}')
                        ->where(['sku_number' => $skuNumber])
                        ->exists($this->db);
    }

    public function update(StorageUnit $unit)
    {
        $this->db->createCommand()->update(
                '{{%storage_unit}}',
                [
                    'title' => $unit->getTitle(),
                    'price' => $unit->getPrice(),
                    'characteristics' => json_encode($unit->getCharacteristics()),
                ],
                ['sku_number' => $unit->getSkuNumber()]
        )->execute();
    }

    public function find($skuNumber): StorageUnit
    {
        $row = $this->query
                ->from('{{%storage_unit}}')
                ->where(['sku_number' => $skuNumber])
                ->one();
        if(!$row)
        {
            throw new \RobotE13\StorageAccounting\Repositories\NotFoundException("Storage unit with sku number {$skuNumber} not found.");
        }
        return $this->populate($row);
    }

    public function remove($name)
    {

    }

    protected function populate($row): StorageUnit
    {
        return new StorageUnit(
                $row['sku_number'],
                $row['title'],
                $this->unitTypes->find($row['sku_type']),
                $row['status'],
                json_decode($row['characteristics'], true),
                $row['preview'],
                $row['price'],
                $row['available']
        );
    }

}
