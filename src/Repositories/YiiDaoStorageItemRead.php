<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Repositories;

use Yii;
use JzWebstudio\Yii2StorageAccounting\models\StorageItemQuery;
use yii\db\Connection;
use yii\data\ActiveDataProvider;
use RobotE13\StorageAccounting\Services\StorageItem\Get\GetStorageItems;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemReadRepository;

/**
 * Description of YiiDaoStorageItemRead
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class YiiDaoStorageItemRead implements StorageItemReadRepository
{

    /**
     * @var StorageItemQuery
     */
    private $query;

    /**
     *
     * @var Connection
     */
    private $db;

    public function __construct($db = 'db')
    {
        $this->query = \JzWebstudio\Yii2StorageAccounting\models\StorageItem::find();
        $this->query->alias('storage_item')
//                ->select('storage_item.*,storage_unit.*')
                ->joinWith(['storageUnits' => fn($query)=>$query->alias('storage_unit')])
                ->with('defaultVariant');
        $this->db = Yii::$app->get($db);
    }

    /**
     *
     * @param GetStorageItems $query
     * @return \yii\db\BatchQueryResult
     */
    public function getAll(GetStorageItems $query)
    {
        $this->buildQuery($query);
        return $this->query->batch($query->getBatchSize(), $this->db);
    }

    /**
     *
     * @param GetStorageItems $query
     * @return \yii\data\ArrayDataProvider
     */
    public function getPagination(GetStorageItems $query)
    {
        $this->buildQuery($query);
        $pagination = $query->getBatchSize() > 0 ? ['pageSize' => $query->getBatchSize()] : false;
        $count = clone $this->query;
        return new ActiveDataProvider([
            'db' => $this->db,
            'query' => $this->query,
            'pagination' => $pagination,
            'totalCount' => $count->limit(-1)->offset(-1)->orderBy([])->select('uid')
                ->count('uid', $this->db)
        ]);
    }

    private function buildQuery(GetStorageItems $query): void
    {
        $this->query->andFilterWhere([
                    'uid' => $query->getUid(),
                    'sku_number' => $query->getSkuNumber()
                ])
                ->andFilterWhere(['like', 'slug', $query->getSlug()])
                ->andFilterWhere(['like', 'storage_item.title', $query->getTitle()])->indexBy('uid')->groupBy('uid');
    }

}
