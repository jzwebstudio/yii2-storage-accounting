<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use Webmozart\Assert\Assert;
use yii\helpers\ArrayHelper;
use RobotE13\StorageAccounting\Services\StorageItem\Update\UpdateItem;

/**
 * Description of UpdateItemForm
 *
 * @property-read string $uid
 * @property StorageItemForm $item Description
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateItemForm extends \elisdn\compositeForm\CompositeForm
{

    /**
     * @var string
     */
    protected $_uid;

    public function __construct(string $uid, $config = array())
    {
        Assert::notEmpty($uid);
        $this->_uid = $uid;
        $this->item = new StorageItemForm(ArrayHelper::getValue($config, 'item', []));
        $this->item->scenario = StorageItemForm::SCENARIO_UPDATE;

        parent::__construct($config);
    }

    protected function internalForms(): array
    {
        return ['item'];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
                    'uid' => fn() => $this->_uid,
                    'item' => fn() => array_filter($this->item->attributes, fn($value) => isset($value))
        ]);
    }

    public function getUid(): string
    {
        return $this->_uid;
    }

    /**
     * @return UpdateItemAttribute
     */
    public function getCommand()
    {
        return new UpdateItem($this->_uid, array_filter($this->item->attributes, fn($attribute) => isset($attribute)));
    }

}
