<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use Yii;

/**
 * Description of StorageUnitForm
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageUnitForm extends \yii\base\Model
{

    const SCENARIO_UPDATE = 'update';

    /**
     * @var string SKU number
     */
    public $number;

    /**
     * @var string unit title
     */
    public $title;

    /**
     * @var string unit type name {@see \RobotE13\StorageAccounting\Entities\StorageUnitType}
     */
    public $type;
    public $status;
    public $characteristics = [];

    /**
     * @var string url to preview
     */
    public $preview;
    public $price;

    public function rules(): array
    {
        return[
            [['title', 'number', 'type'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['title'], 'filter', 'filter' => 'strip_tags','skipOnEmpty'=>true],
            [['preview'], 'string'],
            [['price'], 'filter', 'filter' => fn($value) => mb_ereg_replace('/[^\d\.]/', '', $value),'skipOnEmpty'=>true],
            [['status'], 'integer', 'min' => 0, 'max' => 1],
            [['characteristics'], 'safe']
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['title', 'price','characteristics']
        ]);
    }

    public function attributeLabels(): array
    {
        return[
            'number' => Yii::t('jzwebstudio/storage', 'SKU Number'),
            'title' => Yii::t('jzwebstudio/storage', 'Storage unit title'),
            'type' => Yii::t('jzwebstudio/storage', 'Unit type name'),
            'preview' => Yii::t('jzwebstudio/storage', 'Badge'),
            'price' => Yii::t('jzwebstudio/storage', 'Price')
        ];
    }

    public function formName()
    {
        return 'Unit';
    }

}
