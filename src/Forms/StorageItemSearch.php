<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use Yii;
use RobotE13\StorageAccounting\Services\StorageItem\Get\GetStorageItems;

/**
 * Description of StorageItemSearch
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItemSearch extends \yii\base\Model
{

    public $uids;
    public $title;
    public $slug;
    public $skuNumber;
    public $pageSize;

    public function rules(): array
    {
        return [
            [['title','slug','skuNumber'],'string'],
            [['uids'],'each','rule'=>'string'],
            [['pageSize'],'integer']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'uids' => Yii::t('jzwebstudio/storage', 'Item ID\'s'),
            'title' => Yii::t('jzwebstudio/storage', 'Item title'),
            'slug' => Yii::t('jzwebstudio/storage', 'Item slug'),
            'skuNumber' => Yii::t('jzwebstudio/storage', 'SKU Number'),
        ];
    }

    public function getCommand()
    {
        return new GetStorageItems(
                (array)$this->uids ?? [],
                $this->title ?? '',
                $this->slug ?? '',
                $this->skuNumber ?? '',
                $this->pageSize ?? 20
        );
    }

    public function formName()
    {
        return '';
    }

}
