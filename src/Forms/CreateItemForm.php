<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use yii\helpers\ArrayHelper;
use RobotE13\StorageAccounting\Services\StorageItem\Create\CreateItem;

/**
 * Description of CreateItemForm
 *
 * @property StorageItemForm $item Description
 * @property StorageUnitForm $unit Description
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CreateItemForm extends \elisdn\compositeForm\CompositeForm
{

    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->item = new StorageItemForm(ArrayHelper::getValue($config, 'item', []));
        $this->unit = new StorageUnitForm(ArrayHelper::getValue($config, 'unit', []));
    }

    protected function internalForms(): array
    {
        return['item', 'unit'];
    }

    public function getCommand(): CreateItem
    {
        if(!$this->validate())
        {
            throw new \InvalidArgumentException(array_reduce($this->getFirstErrors(), fn($prev, $next) => $prev . $next));
        }
        return new CreateItem(
                $this->item->title,
                $this->item->slug,
                $this->unit->number,
                $this->unit->type,
                $this->unit->status,
                $this->item->description ?? '',
                $this->unit->preview ?? '',
                (float) $this->unit->price,
                $this->unit->characteristics
        );
    }

}
