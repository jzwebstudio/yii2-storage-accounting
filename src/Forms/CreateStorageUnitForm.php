<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use RobotE13\StorageAccounting\Services\StorageItem\AddSkuVariant\AddSkuVariant;

/**
 * Description of CreateStorageUnitForm
 * @property StorageUnitForm $unit Description
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CreateStorageUnitForm extends \elisdn\compositeForm\CompositeForm
{

    public $storageItemId;

    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->unit = new StorageUnitForm(\yii\helpers\ArrayHelper::getValue($config,'unit',[]));
    }

    protected function internalForms(): array
    {
        return ['unit'];
    }

    public function getCommand(): AddSkuVariant
    {
        if(!$this->validate())
        {
            throw new \InvalidArgumentException(array_reduce($this->getFirstErrors(),fn($prev,$next)=>$prev . $next));
        }
        return new AddSkuVariant(
                $this->storageItemId,
                $this->unit->number,
                $this->unit->title,
                $this->unit->type,
                $this->unit->status,
                $this->unit->characteristics,
                $this->unit->preview,
                (float)$this->unit->price
        );
    }

}
