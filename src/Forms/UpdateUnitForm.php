<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use yii\helpers\ArrayHelper;
use RobotE13\StorageAccounting\Services\StorageUnit\Update\UpdateStorageUnit;

/**
 * Description of ChangePriceForm
 *
 * @property-read StorageUnitForm $unit Description
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateUnitForm extends \elisdn\compositeForm\CompositeForm
{

    private $skuNumber;

    public function __construct($skuNumber, $config = array())
    {
        $this->skuNumber = $skuNumber;
        $this->unit = new StorageUnitForm(ArrayHelper::getValue($config, 'unit', []));
        $this->unit->scenario = StorageUnitForm::SCENARIO_UPDATE;
        parent::__construct($config);
    }

    protected function internalForms(): array
    {
        return ['unit'];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
                    'sku' => fn() => $this->skuNumber,
                    'unit' => fn() => array_filter($this->unit->attributes, fn($value) => isset($value))
        ]);
    }

    public function getCommand(): UpdateStorageUnit
    {
        return new UpdateStorageUnit($this->skuNumber,
                array_filter($this->unit->getAttributes(null, ['characteristics']), fn($attribute) => isset($attribute)),
                $this->unit->characteristics
        );
    }

}
