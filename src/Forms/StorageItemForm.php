<?php

/**
 * This file is part of the yii2-storage-accounting.
 *
 * Copyright 2020 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-storage-accounting
 */

namespace JzWebstudio\Yii2StorageAccounting\Forms;

use Yii;

/**
 * Description of StorageItemForm
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItemForm extends \yii\base\Model
{

    const SCENARIO_UPDATE = 'update';

    public $title;
    public $slug;
    public $description;
    public $defaultVariant;

    public function rules(): array
    {
        return[
            [['title', 'slug'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['title'], 'filter', 'filter' => 'strip_tags', 'skipOnEmpty' => true],
            [['slug'], 'match', 'pattern' => '/^[A-Za-z\d\-\_]+$/'],
            [['description'], 'filter', 'filter' => fn($value) => strip_tags($value, '<p><div><span><ul><ol><li><b><strong><i><s><br><h2><h3><h4><h5><h6>')]
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [self::SCENARIO_UPDATE => ['title', 'slug', 'description']]);
    }

    public function attributeLabels(): array
    {
        return[
            'title' => Yii::t('jzwebstudio/storage', 'Item title'),
            'slug' => Yii::t('jzwebstudio/storage', 'Item slug'),
            'description' => Yii::t('jzwebstudio/storage', 'Item description'),
            'defaultVariant' => Yii::t('jzwebstudio/storage', 'Default SKU')
        ];
    }

    public function formName()
    {
        return 'Item';
    }

}
